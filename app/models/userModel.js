const mongoose = require ("mongoose");

//import thư viện schema
const Schema = mongoose.Schema;

//khởi tạo user
const userSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    age: {
        type: Number,
        default: 0
    },
    cars: [{
        type: mongoose.Types.ObjectId,
        ref: "Car"
    }]
});

module.exports = mongoose.model("User", userSchema);