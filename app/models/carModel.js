const mongoose = require ("mongoose");

//import thư viện schema
const Schema = mongoose.Schema;

//khởi tạo car
const carSchema = new Schema ({
    model:{
        type: String,
        required: true
    },
    vId: {
        type: String,
        required: true,
        unique: true
    }
})

module.exports = mongoose.model ("Car",carSchema);