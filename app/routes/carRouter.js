const express = require("express");

const carController = require ("../controllers/carController");

const router = express.Router();

router.post("/users/:userId/cars",carController.createCar);
router.get("/cars",carController.getAllCar);
router.get("/users/:userId/cars", carController.getAllCarDetailOfUser);
router.get("/cars/:carId", carController.getCarById);
router.put("/cars/:carId", carController.updateCar);
router.delete("/users/:userId/cars/:carId", carController.deleteCarDetail);


module.exports = router;