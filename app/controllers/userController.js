const mongoose = require("mongoose");
const userModel = require("../models/userModel");

//function create user
const createUser = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    if(!body.name){
        return response.status(400).json({
            status:"Bad request",
            message:"Name không hợp lệ"
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status:"Bad request",
            message:"Phone không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        phone: body.phone,
        age: body.age,
        cars: body.car
    }
    userModel.create(newUser, (error,data)=>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
            return response.status(201).json({
                status: "Create new user successfully",
                data: data
            })
    })
}
//function get all users
const getAllUser = (request, response) =>{
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    userModel.find((error,data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all users successfully",
            data: data
        })
    })
}
//function get user by Id
const getUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findById(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail user successfully",
            data: data
        })
    })
}
//function updateUserById
const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user Id không hợp lệ"
        })
    }

    if(body.name !== undefined && body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }

    if(body.phone !== undefined && body.phone.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Phone không hợp lệ"
        })
    }


    // B3: Gọi Model tạo dữ liệu
    const updateUser = {}

    if(body.name !== undefined) {
        updateUser.name = body.name
    }

    if(body.phone !== undefined) {
        updateUser.phone = body.phone
    }

    if(body.age !== undefined) {
        updateUser.age = body.age
    }

    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update user successfully",
            data: data
        })
    })
}
//function deleteUserById
const deleteUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete user successfully"
        })
    })
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}