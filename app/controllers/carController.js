//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo car model
const carModel = require("../models/carModel");
//import user model
const userModel = require("../models/userModel");

//function create new car detail
const createCar = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    const userId = request.params.userId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status: "Bad request",
            message: "car Id không hợp lệ"
        })
    }
    if(!body.model){
        return response.status(400).json({
            status:"Bad request",
            message:"model không hợp lệ"
        })
    }if(!body.vId){
        return response.status(400).json({
            status:"Bad request",
            message:"vId không hợp lệ"
        })
    }
    //B3: thao tác với database
    const newCar = {
        _id: mongoose.Types.ObjectId(),
        model: body.model,
        vId: body.vId
    }
    carModel.create(newCar,(error, data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //thêm Id của car  mới được tạo vào mảng user được liên kêt qua ref Car
        userModel.findByIdAndUpdate(userId,{
            $push: {
                cars: data._id
            }
        }, (err, updateUser) =>{
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status:"Created new Car Detail successfully",
                data: data
            })
        })
    })

}
//function get all car detail of user
const getAllCarDetailOfUser = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const userId = request.params.userId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status: "Bad request",
            message: "User Id không hợp lệ"
        })
    }
    //B3: xử lý database từ user model
    userModel.findById(userId)
            .populate("cars")
            .exec((error, data) =>{
                if(error){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: error.message
                    })
                }
                return response.status(200).json({
                    status:" Get Car Detail by Id successfully",
                    data: data
                })
            })
}
//function get all car
const getAllCar  = (request, response) => {
    carModel.find((error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all car successfully",
            data:data
        })
    })
}
//function get car by id
const getCarById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const carId = request.params.carId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Car Id không hợp lệ"
        })
    }
    //B3: gọi model car chứa car id
    carModel.findById(carId, (error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get car detail successfully",
            data: data
        })
    })
}
//function update car by id
const updateCar = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const carId = request.params.carId;
    const body = request.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)){
        return response.status(400).json({
            status:" Bad request",
            message: " car Id không hợp lệ"
        })
    }
    //B3: thao tác với database
    const updateCarDetail = {};
    if(body.model !== undefined){
        updateCarDetail.model = body.model
    }
    if(body.vId !== undefined){
        updateCarDetail.vId = body.vId
    }

    carModel.findByIdAndUpdate(carId, updateCarDetail, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message:error.message
            })
        }
        return response.status(200).json({
            status:"Update car details successfully",
            data: data
        })
    })
}
//function delete detail by car id
const deleteCarDetail = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const carId = request.params.carId;
    const userId = request.params.userId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status: " Bad request",
            message: "user ID không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(carId)){
        return response.status(400).json({
            status: "Bad request",
            message: "car id không hợp lệ"
        })
    }
    carModel.findByIdAndDelete(carId,(error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        userModel.findByIdAndUpdate(userId,{
            $pull: {cars: carId}
        }, (err, updateUser) => {
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(200).json({
                status: "Delete Car successfully"
            })
        })
    })
}
module.exports = {
    createCar,
    getAllCarDetailOfUser,
    getAllCar,
    getCarById,
    updateCar,
    deleteCarDetail
}