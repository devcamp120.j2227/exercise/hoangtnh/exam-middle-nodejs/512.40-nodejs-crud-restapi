//import thư viện express JS
const express = require ("express");

//import thư viện mongoose
const mongoose = require("mongoose");

//khởi tạo app chạy
const app = express();

//khai báo cổng chạy
const port = 8000;
//kết nối với mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_middleExam", (error) => {
    if(error) throw error;
    console.log("Connect User to MongoDB successfully!");
});
// Cấu hình request đọc được body json
app.use(express.json());

//khai báo router app
const userRouter = require("./app/routes/userRouter");
const carRouter = require("./app/routes/carRouter");

//app sử dụng router
app.use("/api",userRouter);
app.use("/api",carRouter);

//app chạy trên port
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})